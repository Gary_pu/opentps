opentps.core.examples package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   opentps.core.examples.doseComputation
   opentps.core.examples.doseDeliverySimulation
   opentps.core.examples.dynamicData
   opentps.core.examples.imageProcessing
   opentps.core.examples.planOptimization
   opentps.core.examples.registration
   opentps.core.examples.segmentation

Submodules
----------

opentps.core.examples.syntheticData module
------------------------------------------

.. automodule:: opentps.core.examples.syntheticData
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: opentps.core.examples
   :members:
   :undoc-members:
   :show-inheritance:

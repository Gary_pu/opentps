opentps.gui.examples.extension.extension package
================================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   opentps.gui.examples.extension.extension.gui

Submodules
----------

opentps.gui.examples.extension.extension.main module
----------------------------------------------------

.. automodule:: opentps.gui.examples.extension.extension.main
   :members:
   :undoc-members:
   :show-inheritance:

opentps.gui.examples.extension.extension.utils module
-----------------------------------------------------

.. automodule:: opentps.gui.examples.extension.extension.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: opentps.gui.examples.extension.extension
   :members:
   :undoc-members:
   :show-inheritance:
